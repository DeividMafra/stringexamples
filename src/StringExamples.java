
public class StringExamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//initial commit

		// 1. Create a string
		String name = "peter sigurdson";
		System.out.println(name);
		
		// 2. Number of characters in the string
		int numChars = name.length();
		System.out.println("Number of characters: "+ numChars);
		
		// 3. Get a specific character
		char charAtPosition4 = name.charAt(4);
		System.out.println("Char at position 4: "+ charAtPosition4);
		
		// 4. Get a substring
		String sub = name.substring(0,5);
		System.out.println("Get substring 1: "+ sub);
		
		String sub2 = name.substring(7);
		System.out.println("Get substring 2: "+ sub2);
			
		// 5. Check if one string is equal to another
		String a = "emad";
		String b = "mohamed";
		String c = "emad";
		
		if (a.contentEquals(c)) {
			System.out.println("a and c are equals");
		}else {
			System.out.println("a and c are NOT equals");
		}
		
//		do an OBJECT comparison between a & c
//		compare emad and emad
		if (a == c) {
			System.out.println("a1 and c1 are equals");
		}else {
			System.out.println("a1 and c1 are NOT equals");
		}
		
		// 6. Make everything uppercase
		String upper = name.toUpperCase();
		System.out.println(upper);
		
		// 7. Make everything lowercase
		String lower = name.toLowerCase();
		System.out.println(lower);
		
	}

}
